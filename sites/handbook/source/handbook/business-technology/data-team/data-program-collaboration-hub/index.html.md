---
layout: handbook-page-toc
title: "Data Program Collaboration Hub"
description: "Data and Analytics oriented meetings, initiatives, and people"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

---

## <i class="fas fa-users fa-fw color-orange font-awesome" aria-hidden="true"></i>Data Program Collaboration Hub

Here is a reference for the Data Program Teams meeting series, subject DRIs, slack channels, and initiatives.


|	**TEAM**	|	**PRIMARY PARTNERS**	|	**PRIMARY SLACK CHANNEL**	|	**WEEKLY MEETING**	|	**BI-WEEKLY MEETING**	|	**MONTHLY MEETING**	|	**QUARTERLY MEETING**	|
|	:---------------	|	:---------------	|	:---------------	|	:---------------	|	:---------------	|	:---------------	|	:---------------	|
|	[Marketing](/handbook/marketing/)	|		|		|		|		|		|		|
|	[Marketing - Operations](/handbook/marketing/marketing-operations/)	|	Amy	|	#marketing-data-ops	|		|	X	|		|		|
|	[Marketing - Strategy & Performance](/handbook/marketing/strategy-performance/)	|	Jerome, Christine	|	#data-gtm-projects	|		|		|		|		|
|	Marketing - SDR	|		|	no dedicated channel	|		|		|		|		|
|	Marketing - Field	|		|	no dedicated channel	|		|		|		|		|
|	Marketing - Corporate	|		|	no dedicated channel	|		|		|		|		|
|	[Marketing - Campaigns / Demand Gen](/handbook/marketing/demand-generation/campaigns/)	|	Jackie	|	no dedicated channel	|		|		|		|		|
|	Marketing - Digital Experience	|		|	no dedicated channel	|		|		|		|		|
|	Marketing - Community Relations	|		|	no dedicated channel	|		|		|		|		|
|	Marketing - Portfolio	|		|	no dedicated channel	|		|		|		|		|
|	[Sales](/handbook/sales/)	|		|		|		|		|		|		|
|	Sales - New	|		|	no dedicated channel	|		|		|		|		|
|	Sales - Customer Success	|	David	|	various / project-based	|		|		|		|		|
|	[Sales - Customer Success Operations](/handbook/sales/field-operations/customer-success-operations/)	|	Jeff, Emily	|	#wg-gtm-product-analytics	|		|		|	X (x-functional series)	|		|
|	[Sales - Strategy & Analytics](/handbook/sales/field-operations/sales-strategy/)	|	Jake, Melia, Noel	|	#data-gtm-projects	|		|		|	X (x-functional series)	|		|
|	Sales - Support	|		|	no dedicated channel	|		|		|		|		|
|	Sales - Field Operations	|		|	no dedicated channel	|		|		|		|		|
|	[Product](/handbook/product/)	|		|		|		|		|		|		|
|	[Growth](/direction/growth/)	|	Sam	|	#s_growth_data	|		|		|	X (x-functional series)	|		|
|	[Product Data Insights](/handbook/product/product-analysis/)	|	Carolyn	|	#data	|	X (Wed/Thurs)	|	X (Office Hours)	|	X (x-functional series)	|		|
|	Engineering	|		|		|		|		|		|		|
|	[Product Intelligence](/handbook/engineering/development/analytics/product-intelligence)	|	Amanda, Alina	|	#g_product_intelligence	|		|	X	|	X (x-functional series)	|		|
|	[Engineering Analytics](/handbook/engineering/quality/engineering-analytics/)	|	Mek, Lily	|	#eng-data-kpi	|		|	X	|		|		|
|	People	|		|		|		|		|		|		|
|	[People - Operations, Technology, & Analytics](/handbook/people-group/people-ops-tech-analytics/)	|	Adrian	|	#data-people-projects	|	X	|		|		|		|
|	Finance	|		|		|		|		|		|		|
|	[Sales Finance](/handbook/finance/financial-planning-and-analysis/sales-finance/)	|	Fred	|	#data-gtm-projects	|		|		|		|		|
|	[Analytics & Insights](/job-families/finance/analytics-and-insights/)	|	Sindhu, Charan, Kelly	|	various / project-based	|	X (UCI)	|		|		|		|
|	Corporate Finance	|	James	|	various / project-based	|		|		|		|		|
|	X-Functional Programs	|		|		|		|		|		|		|
|	PI Leadership - Product/Engineering/Data	|	Wayne, Amanda, Phil, Alina	|	no dedicated channel	|	X	|		|		|		|
|	GTM Teams	|	Jake, Jeff, Sindhu, Fred	|	#data-gtm-projects	|		|		|	X	|		|
|	R&D Teams	|	Justin, Hila	|	#data-analytics-rd-projects	|		|	X	|		|		|
|	G&A Teams	|	project based	|	no dedicated channel	|		|		|		|		|
|	PI, Fulfillment, Data, Customer Success, Sales	|	Jeff, Amanda, Emily	|	#wg-gtm-product-analytics	|		|	X	|		|		|
|	Data Science	|	project based	|	#bt-data-science	|	X (per project)	|		|		|		|
|	Data Team	|		|		|		|		|		|		|
|	Data Science	|	N/A	|	#bt-data-science	|	X (Tues)	|		|		|		|
|	Data Platform	|	N/A	|	#data-engineering	|	X (Tues)	|		|		|		|
|	Data R&D Fusion	|	N/A	|	#data-rd-fusion	|	X (Tues)	|		|		|		|
|	Data GTM Fusion	|	N/A	|	#data-gtm-projects	|	X (Tues)	|		|		|		|
|	Data G&A Fusion	|	N/A	|	no dedicated channel	|		|		|		|		|
|	Data Collaboration	|	Carolyn, Noel, Melia, Jerome, Lily	|	#bt-data-collaboration	|	X (Tues)	|		|		|		|
|	[Functional Analytics Center of Excellence (FACE)](/handbook/business-technology/data-team/functional-analytics-center-of-excellence/)	|	Alex, Sindhu, Jerome, Melia, Noel, Michael, Dennis, Adrian, Marcus, Carolyn	|	#functional_analytics_center_of_excellence	|		|	X (Thurs)	|		|		|
