The issue tracks the adoption of [Next Prioritization Working Group](https://about.gitlab.com/company/team/structure/working-groups/next-prioritization/) for a specific group.

Each group will have an issue to tag the members of the quad for the group and track actions the group must take to adopt the new cross-functional prioritization process.

# Who

| Role | GitLab user id |
| --- | --- |
| Product Manager | TBD |
| Engineering development manager | TBD |
| UX stable counterpart | TBD |
| Quality stable counterpart | TBD |

# Steps

Please check off these as they are verified/completed

* [ ] Tag GitLab user names for the product manager, engineering development manager, UX stable counterpart, and quality stable counterpart for this group in the table above
* [ ] Change name of issue to : "Group (group name)"
* [ ] A dashboard is available in the handbook showing the % of MRs that are bugs vs maintenance vs features
* [ ] A review cadence has been created to review and discuss the dashboard monthly.
* [ ] The development manager is prioritizing maintenance work and has done it for at least one milestone
* [ ] The quality manager is prioritizing quality work and has done it for at least one milestone
* [ ] Once all items above are verified/completed, close the issue (issue state is being used to determine adoption trends of the new process)


/label ~"wg-next-prioritization" ~"wg-next-prioritization-adoption" 

