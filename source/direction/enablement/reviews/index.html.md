---
layout: markdown_page
title: Enablement Direction Page reviews
description: "The bi-weekly direction review schedule."
canonical_path: "/direction/enablement/reviews/"
---

## What's on this page

This page contains the bi-weekly Enablement & SaaS Platforms section direction review. Every other week, the PM teams reviews one of the groups direction pages asynchronously and adds their questions to the weekly PM agenda discussion. During the weekly meeting, these questions are discussed synchronously and recorded separately from the main meeting.

### Schedule

| Group | Date | PM | Recording |
| ----- | ---- | -- | --------- |
| TBD | 2022-06-01 | TBD | N/A |
| [Global Search](https://about.gitlab.com/direction/global-search/) | 2022-06-15 | [John McGuire](https://gitlab.com/johnmcguire) | N/A |
| TBD | 2022-06-29 | TBD | N/A |
| [Geo](https://about.gitlab.com/direction/geo/) | 2022-07-13 | [Sampath Ranasinghe](https://gitlab.com/sranasinghe) | N/A |
| TBD | 2022-07-27 | TBD | N/A |
| TBD | 2022-08-10 | TBD | N/A |
| [Code Search Category](https://about.gitlab.com/direction/global-search/code-search/) | 2022-08-24 | [John McGuire](https://gitlab.com/johnmcguire) | N/A |
